<?php

require_once '../src/SomeClass.php';

class StubTest extends PHPUnit_Framework_TestCase
{
    //Crear un stub que devuelve valor fijo
    public function testStub()
    {
	// Crear el stu para SomeClass
	$stub = $this->getMockBuilder('SomeClass')
             ->getMock();

	// Configurar el stub
	$stub->method('doSomething')
     		->willReturn('foo');

	// llamar  $stub->doSomething() retornara 'foo'.
	$this->assertEquals('foo', $stub->doSomething());
    } 

    //Devuelve uno de los argumentos
    public function testReturnArgumentStub()
    {
        // Crear el stub
        $stub = $this->getMockBuilder('SomeClass')
                     ->getMock();

        // Configurarlo para devolver el primer parametro
         $stub->method('doSomething')
             ->will($this->returnArgument(0));

        // $stub->doSomething('foo') retorna foo
        $this->assertEquals('foo', $stub->doSomething('foo'));

        // $stub->doSomething('bar') retorna bar
        $this->assertEquals('bar', $stub->doSomething('bar'));
    }


    //Devuelve el mismo stub
    public function testReturnSelf()
    {
        // Crea el stub
        $stub = $this->getMockBuilder('SomeClass')
                     ->getMock();
       
        // Configurar el stub para se devuelva a si mismo
        $stub->method('doSomething')
             ->will($this->returnSelf());

        // $stub->doSomething() retorna el stub
        $this->assertSame($stub, $stub->doSomething());
    }

    //Devuelve un valor dependiendo de un mapa de valores
    public function testReturnValueMapStub()
    {
        // Crear els stub
        $stub = $this->getMockBuilder('SomeClass')
                     ->getMock();

        // Crear el mapa de valores
        $map = array(
          array('a', 'b', 'a+b'),
          array('c', 'd', 'c+d')
        );
        
        // Configurar el stub para usar el mapa
        $stub->method('doSomething')
             ->will($this->returnValueMap($map));

        // $stub->doSomething() retorna diferente valores dependiendo 
        // de los argumentos
        $this->assertEquals('a+b', $stub->doSomething('a','b'));
        $this->assertEquals('c+d', $stub->doSomething('c','d'));
    }

    // retorna un valor desde un callback
    public function testReturnCallbackStub()
    {
        // Crear el stub
        $stub = $this->getMockBuilder('SomeClass')
                     ->getMock();

        // Configurar el stub.
        $stub->method('doSomething')
             ->will($this->returnCallback('StubTest::stubCallback'));
        
       // $stub->doSomething($argument) retorna stubCallback($argument)
        $this->assertEquals('something', $stub->doSomething('something'));
    }

    function stubCallback($argument)
    {
    	return $argument;
    } 

    //Devuelve valores especificos para cada llamada
    public function testOnConsecutiveCallsStub()
    {
	// Crear stub
	$stub = $this->getMockBuilder('SomeClass')
             ->getMock();

	// Configurar el stub.
	$stub->method('doSomething')
     		->will($this->onConsecutiveCalls(2, 3, 5, 7));

	// $stub->doSomething() retorna diferentes valores cada llamada
	$this->assertEquals(2, $stub->doSomething());
	$this->assertEquals(3, $stub->doSomething());
	$this->assertEquals(5, $stub->doSomething());
    }

    //Genera una excepcion
    public function testThrowExceptionStub()
    {
        // Crear el stub
        $stub = $this->getMockBuilder('SomeClass')
                     ->getMock();

        // Configurar el stub
        $stub->method('doSomething')
             ->will($this->throwException(new Exception));

        // $stub->doSomething() genera una excepcion
        //$stub->doSomething();
    }
}
?>
